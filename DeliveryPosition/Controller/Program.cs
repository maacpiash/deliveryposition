﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.IO;
using System.Text;
using OfficeOpenXml;
using System.Windows.Forms;
using DeliveryPosition.View;
using DeliveryPosition.Model;

namespace DeliveryPosition
{
    static class Program
    {
        public static List<List<Delivery>> deliveries;
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            deliveries = new List<List<Delivery>>();
            FileInfo fileInfo = new FileInfo("C:/Users/maacp/Documents/AKM Knit Wear.xlsx");
            using (var package = new ExcelPackage(fileInfo))
            {
                //int max = package.Workbook.Worksheets.Count;
                //for (int i = 1; i < max; i++)
                    //deliveries.Add(ReadAllDeliveriesFromWorksheet(package.Workbook.Worksheets[1]).ToList());
            }

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }

        public static Delivery ReadOneDelivery(ExcelWorksheet worksheet, int row)
        {
            var rowCount = worksheet.Dimension?.Rows;
            var colCount = worksheet.Dimension?.Columns;

            if (!rowCount.HasValue || !colCount.HasValue)
            {
                return null;
            }

            string[] values = new string[12];
            for (int col = 1; col <= 12; col++)
            {
                try { values[col - 1] = worksheet.Cells[row, col].Value.ToString(); }
                catch { values[col - 1] = "[Empty]"; }
            }

            return new Delivery(values);


        }
    }
}

