﻿using System;

namespace DeliveryPosition.Model
{
    class Delivery
    {
        #region Proerties
        public string LCn { get => lcn; set => lcn = value; }
        private string lcn;

        public string PIn { get => pi; set => pi = value; }
        private string pi;

        public DateTime Date { get => dt; set => dt = value; }
        private DateTime dt;

        public string Buyer { get => buyer; set => buyer = value; }
        private string buyer;

        public string Merchandiser { get => merchandiser; set => merchandiser = value; }
        private string merchandiser;

        public string Construction { get => construction; set => construction = value; }
        private string construction;

        public string StyleRefNum { get => stylerefnum; set => stylerefnum = value; }
        private string stylerefnum;

        public string ColorNum { get => colornum; set => colornum = value; }
        private string colornum;

        public int Order { get => order; set => order = value; }
        private int order;

        public int Delivered { get => delivered; set => delivered = value; }
        private int delivered;

        public int Balance { get => balance; set => balance = value; }
        private int balance;

        public string Remarks { get => remarks; set => remarks = value; }
        private string remarks;
        #endregion

        public Delivery(params string[] stuff)
        {
            try
            {
                #region Inputs From Array
                int i = 0;
                lcn = stuff[i++];
                pi = stuff[i++];
                dt = Convert.ToDateTime(stuff[i++]);
                buyer = stuff[i++];
                merchandiser = stuff[i++];
                construction = stuff[i++];
                stylerefnum = stuff[i++];
                colornum = stuff[i++];
                order = Convert.ToInt32(stuff[i++]);
                delivered = Convert.ToInt32(stuff[i++]);
                balance = Convert.ToInt32(stuff[i++]);
                #endregion
            }
            catch (Exception x)
            {
                Console.WriteLine(x.GetType().ToString());
            }
        }
        
        public string[] ToStringArray()
        {
            string[] values = new string[12];
            #region values array being set
            int i = 0;
            values[i++] = lcn;
            values[i++] = pi;
            values[i++] = dt.ToShortDateString();
            values[i++] = buyer;
            values[i++] = merchandiser;
            values[i++] = construction;
            values[i++] = stylerefnum;
            values[i++] = colornum;
            values[i++] = order.ToString();
            values[i++] = delivered.ToString();
            values[i++] = balance.ToString();
            values[i++] = remarks;
            #endregion
            return values;
        }
    }
}
